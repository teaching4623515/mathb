\documentclass{article}

% Language setting
% Replace `english' with e.g. `spanish' to change the document language
\usepackage[czech]{babel}

% Set page size and margins
% Replace `letterpaper' with `a4paper' for UK/EU standard size
\usepackage[letterpaper,top=2cm,bottom=2cm,left=3cm,right=3cm,marginparwidth=1.75cm]{geometry}

% Useful packages
\usepackage{amsmath}
\usepackage{graphicx}
\usepackage[colorlinks=true, allcolors=blue]{hyperref}
\usepackage{empheq}
\usepackage{amsthm}
\usepackage{amssymb}
\usepackage{bm}
\usepackage{color}
\usepackage{xcolor}

\include{format}

\setcounter{section}{11}

\graphicspath{{figs/}}

\begin{document}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% 11. CVICENI
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\SheetTitle{Matematika B}{11}{Jaroslav Schmidt}

\abst{Nezávislost křivkového integrálu na integrační cestě. Potenciál vektorového pole. Určování potenciálu v $\mathbb{R}^2$ a v $\mathbb{R}^3$. Neučí se pojem jednoduše souvislá oblast, místo ní uvádíme slabší větu pro konvexní oblast.}

%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%% TEORIE
%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Definice potencionality}
%
\noindent\fbox{%
    \parbox{\textwidth}{%
        \framebox[1.0\columnwidth]{Potenciální vektorové pole}\nonumber
        \vspace{2mm}
        Vektorové pole $\vect{F}$ se nazývá potenciální, pokud lze postupně všechny jeho složky získat příslušnou derivací určité skalární funkce $U$, kterou nazýváme potenciálem. Např. pro dvourozměrný případ musí platit
        \begin{align*}
            F_1=\frac{\partial U}{\partial x}, \quad F_2=\frac{\partial U}{\partial y}, \quad \text{pro } \vect{F}=(F_1, F_2)
        \end{align*}
        Podobně pro tří dimensionální pole $\vect{F}$ platí, že je potenciální pokud
        \begin{align*}
            F_1=\frac{\partial U}{\partial x}, \quad F_2=\frac{\partial U}{\partial y}, \quad
            F_3=\frac{\partial U}{\partial z}, \quad \text{pro } \vect{F}=(F_1, F_2, F_3)
        \end{align*}
        Potenciál je tedy v jakémsi smyslu nadřezen vektorovému poli, protože tato jedna skalární funkce plně determinuje celé vektorové pole. Na potenciál lze například nahlížet jako jako energii, to je jedna z možných interpretací z fyzikálního pohledu.
        
        \vspace{3mm}
        Zda-li je pole potenciální lze i v případě, že neznáme přímo potenciál. Pole $\vect{F}$ je potenciální, pokud platí 
		\begin{align*}
			\frac{\partial F_1}{\partial y} = \frac{\partial F_2}{\partial x}, \quad \text{pro } n=2 \\ 
			\frac{\partial F_1}{\partial y} = \frac{\partial F_2}{\partial x}, \frac{\partial F_1}{\partial z} = \frac{\partial F_3}{\partial x}, \frac{\partial F_2}{\partial z} = \frac{\partial F_3}{\partial y}, \quad \text{pro } n=3,
		\end{align*}
		kde $n$ je dimenze vektorového pole.
    }%
}

%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%% PŘÍKLAD
%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{p}
	Ověřte, že funkce $U(x,y)=x^2y + \frac{y^3}{3}$ je potenciálem vektorového pole $\vect{F}(x,y)=(2xy, x^2+y^2)$
\end{p}
\begin{s}
	Definičním oborem je $\mathbb{R}^2$ a na celém tomto definičním oboru platí $\frac{\partial U}{\partial x}=F_1$ a $\frac{\partial U}{\partial y}=F_2$, tedy vektorové pole je potenciální.
\end{s}

%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%% PŘÍKLAD
%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{p}
	Ověřte, že funkce $U(x,y)=2\ln x + \cos{xy}$ je potenciálem vektorového pole $\vect{F}(x,y)=(\frac{2}{x} - y\sin{xy}, -x\sin{xy})$
\end{p}
\begin{s}
	Pozor na opatrnou odpověď. Funkce $U$ je sice potenciálem vektorového pole $\vect{F}$, ale jen na množině $\{(x,y)\in\mathbb{R}^2; x>0\}$.
\end{s}

%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%% TEORIE
%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Vlastnosti potenciálního pole}
%
\noindent\fbox{%
    \parbox{\textwidth}{%
        \framebox[1.0\columnwidth]{Nezávislost na integrační cestě}\nonumber
        \vspace{2mm}
        Pokud je vektorové pole $\vect{F}$ potenciální, potom u křivkového integrálu přes toto pole nezávisí na integrační cestě, pouze na poloze počátečního a koncového bodu cesty. Platí navíc
        \begin{align*}
			\int_\mathcal{K}\vect{F}\cdot\dx{\vect{r}}=U(\vect{B}) - U(\vect{A}),
		\end{align*}
		kde $\vect{A}, \vect{B}$ jsou počáteční a koncový bod křivky $\mathcal{K}$. Z výše uvedeného lze navíc vydedukovat, že křivkový integrál přes uzavřenou křivku je roven nule.
    }%
}

%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%% TEORIE
%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Odvození potenciálu}
%
\noindent\fbox{%
    \parbox{\textwidth}{%
        \framebox[1.0\columnwidth]{Odvození potenciálu z vektorového pole}\nonumber
        \vspace{2mm}
        Když zjistíte, že je vektorové pole potenciální a chcete odvodit jeho potenciál, potom lze přímo využít definiční vztah mezi polem a potenciálem a postupně ho integrovat. Postup je pouze rychlým shrnutím, lépe je to vidět z příkladu. Naznačme obecný postup pro pole $\vect{F}(x,y,z)=(F_1, F_2, F_3)$.
        
        \vspace{3mm}
        Prvně vyjdeme z rovnice $\frac{\partial U}{\partial x}(x,y,z)=F_1(x,y,z)$ a zintegrujeme podle $x$. Získáme první odhad potenciálu
        \begin{align*}
			U(x,y,z) = f(x,y,z) + \xi(y,z),
        \end{align*}
        kde $f$ je funkce získaná integrací funkce $F_1$ podle $x$ a $\xi$ hraje roli integrační konstanty. Nyní využijeme druhou informaci $\frac{\partial U}{\partial y}(x,y,z)=F_2(x,y,z)$, dosadíme za $U$ a rovnici zintegrujeme podle $y$, čímz získáme
        \begin{align*}
			\xi(y,z) = g(y,z) + \Omega(z),
        \end{align*}
        kde $g$ je funkce získaná integrací podle $y$ a $\Omega$ hraje roli integrační konstanty. Nakonec využijeme informaci $\frac{\partial U}{\partial z}(x,y,z)=F_3(x,y,z)$, dosadíme za $U$ a rovnici zintegrujeme podle $z$, čímz získáme
        \begin{align*}
			\Omega(z) = h(z) + C,
        \end{align*}
        A potenciální pole je potom rovno
        \begin{align*}
			U(x,y,z) = f(x,y,z) + g(y,z) + h(z) + C,
        \end{align*}
    }%
}

%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%% PŘÍKLAD
%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{p}
	Zjistěte, zda je vektorové pole $F(x,y)=(y^2,x)$ potenciální, a vypočtěte křivkový integrál $I = \int_\mathcal{K}F\mathrm{d}r$, kde $\mathcal{K}$ je část paraboly $y=x^2$ s počátečním bodem $(0,0)$ a koncovým bodem $(1,1)$
\end{p}
\begin{s}
	Vektorové pole není potenciální a $I=\frac{13}{15}$
\end{s}

%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%% PŘÍKLAD
%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{p}
	Ověřte, že vektorové pole $F(x,y)=(2x+y, 3y^2 + x)$ má potenciál na nějaké jednoduše souvislé množině $G$ a najděte potenciál i množinu $G$.
\end{p}
\begin{s}
	Pole $F$ má potenciál $U(x,y)=x^2+y^3+xy$ na množině $G=\mathbb{R}^2$
\end{s}

%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%% PŘÍKLAD
%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{p}
	Ověřte, že vektorové pole $F(x,y, z)=(y^2z^3, 2xyz^3, 3xy^2z^2)$ má potenciál na nějaké jednoduše souvislé množině $G$ a najděte potenciál i množinu $G$.
\end{p}
\begin{s}
	Pole $F$ má potenciál $U(x,y,z)=xy^2z^3$ na množině $G=\mathbb{R}^3$
\end{s}

\end{document}
